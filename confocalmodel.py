# -*- coding: utf-8 -*-
"""
Contains information necessary for import and alignment of confocal microscopy images.

Created on Mon Jan 27 12:22:15 2018

@author: cdw2be

Planar Image Brightness Correction based on: https://imagej.nih.gov/ij/plugins/plane-brightness/2010_Michalek_Biosignal.pdf
"""

# Imports
import math
import tkinter as tk
from tkinter import filedialog
import scipy as sp
import numpy as np
import matplotlib.pyplot as mplt
import glob
from PIL import Image, ImageDraw
import os
import re
from natsort import natsorted, ns
from cardiachelpers import importhelper
from cardiachelpers import confocalhelper
from cardiachelpers import mathhelper
from cardiachelpers import displayhelper

class ConfocalModel():
	"""Model class to hold confocal microscopy images and format them to generate a mesh to align with MRI data.
	"""

	def __init__(self, top_stitching_dir=None, slice_width = 0.5):
		"""Initialize the model made to import confocal microscopy data.
		
		args:
			confocal_dir (string): The path to the directory containing tif image files
		"""
		self.stitching_images = False
		self.slice_width = slice_width
		self.slice_positions = None
		self.data_files = False
		self.fluo_images = {}
		self.model_name = ""
		#self.slices = False
		if top_stitching_dir:
			self.stitching_images = self.setTopStitchingDir(top_stitching_dir)
	
	def setTopStitchingDir(self, top_dir):
		# Get all TIFF files in the directory
		dirs = natsorted([(top_dir + '/' + sub_dir) for sub_dir in os.listdir(top_dir) if os.path.isdir(top_dir + '/' + sub_dir)], alg=ns.IC)
		self.top_dir = top_dir
		self.model_name = os.path.basename(top_dir)
		if not hasattr(self, 'slices') or not self.slices:
			self.slices = [ConfocalSlice() for im_dir in dirs]
			self.slice_positions = [i*self.slice_width for i in range(len(dirs))]
		slice_success = [False]*len(self.slices)
		for i in range(len(self.slices)):
			slice_success[i] = self.slices[i].setStitchingDirectory(dirs[i])
		self.slices = [self.slices[i] for i in range(len(self.slices)) if slice_success[i]]
		self.slice_names = [confocal_slice.slice_name for confocal_slice in self.slices]
		self.model_image = None
		self.slice_image_list = None
		self.endo_contours = None
		self.epi_contours = None
		return(True)
		
	def setStitchedDir(self, stitched_dir):
		self.stitched_files = natsorted(glob.glob(os.path.join(stitched_dir, '*.tif')).copy(), alg=ns.IC)
		if not hasattr(self, 'slices') or not self.slices:
			self.slices = [ConfocalSlice() for stitched_file in self.stitched_files]
			self.slice_positions = [i*self.slice_width for i in range(len(self.stitched_files))]
		for i in range(len(self.stitched_files)):
			self.slices[i].setStitchedImage(self.stitched_files[i])
			
	def setImportDirectory(self, data_dir):
		if not os.path.exists(data_dir):
			print('Directory not found.')
			return(False)
		self.data_files = natsorted(glob.glob(os.path.join(data_dir, '*.cod')).copy(), alg=ns.IC)
		if not hasattr(self, 'slices') or not self.slices:
			self.slices = [ConfocalSlice() for data_file in self.data_files]
			self.slice_positions = [i*self.slice_width for i in range(len(self.data_files))]
		
	def importModelData(self):
		if self.data_files:
			for i in range(len(self.data_files)):
				self.slices[i].importModelData(self.data_files[i])
			return(True)
		else:
			print('No data files available!')
			return(False)
			
	def setExportDirectory(self, data_dir):
		if not hasattr(self, 'slices') or not self.slices:
			return(False)
		for conf_slice in self.slices:
			file_name = conf_slice.slice_name + '.cod'
			full_file_path = os.path.join(data_dir, file_name)
			conf_slice.exportModelData(full_file_path)
	
	def generateStitchedImages(self, slices, sub_slices, channel_list, overlap=0.1, compress_ratio=1, force_file=False):
		"""Adjust image intensity based on edge intensity of adacent images.
		"""
		# Set subslices list to mid-slice if none is indicated.
		for slice_ind, slice_num in enumerate(slices):
			# Determine if the sub_slices list is per-slice, or a general list.
			cur_sub_slices = sub_slices[slice_ind] if isinstance(sub_slices[slice_ind], list) else sub_slices
			for sub_slice in cur_sub_slices:
				for channel_val in channel_list[slice_ind]:
					#file_name = self.top_dir + '/' + self.slice_names[slice_num] + 'Channel' + self.getChannelList(slice_num)[channel_val] + 'Frame' + str(sub_slice) + 'Stitched.tif'
					file_name = self.top_dir + '/' + slice_num + 'Channel' + self.getChannelList(slice_ind)[channel_val] + 'Frame' + str(sub_slice) + 'Stitched.tif'
					if (not os.path.isfile(file_name)) or force_file:
						#self.slices[slice_num].createStitchedImage(channel_num=channel_val, overlap=overlap, compress_ratio=compress_ratio, frame=sub_slice, stitched_file=file_name, force_file=force_file)
						self.slices[slice_ind].createStitchedImage(channel_num=channel_val, overlap=overlap, compress_ratio=compress_ratio, frame=sub_slice, stitched_file=file_name, force_file=force_file)
		
	def generateImageGridFiles(self, slices):
		for slice_ind, slice_num in enumerate(slices):
			#self.slices[slice_num].createImageGridFile()
			self.slices[slice_ind].createImageGridFile()
		
	def getSubsliceList(self, top_slice):
		"""Gather information about which sub-slices are present within each image.
		"""
		return(list(range(self.slices[top_slice].num_slices)))
		
	def getChannelList(self, top_slice):
		"""Gather information about which channels are present within each image.
		"""
		return(list(self.slices[top_slice].channels))
	
	def importModelImage(self, model_image_file):
		self.model_image = model_image_file
		self.slice_image_list = confocalhelper.openModelImage(model_image_file)
	
	def generateContours(self):
		self.endo_contours = [None]*len(self.slice_image_list)
		self.epi_contours = [None]*len(self.slice_image_list)
		for im_num, im_slice in enumerate(self.slice_image_list):
			edge_image = confocalhelper.contourMaskImage(im_slice)
			endo_path, epi_path, labelled_arr = confocalhelper.splitImageObjects(edge_image)
			self.endo_contours[im_num] = confocalhelper.orderPathTrace(endo_path)
			self.epi_contours[im_num] = confocalhelper.orderPathTrace(epi_path)
	
	def prepDataForMesh(self):
		# Calculate slice heights for each slice and 
		slice_heights = []
		endo_contours = []
		epi_contours = []
		pixel_mm_ratios = []
		midpoint_endo_contour = []
		midpoint_pinpoints = []
		for slice_ind, slice_pos in enumerate(self.slice_positions):
			slice_heights.extend([subslice_height+slice_pos for subslice_height in self.slices[slice_ind].slice_positions])
			endo_contours.extend([endo_contours_subslice for endo_contours_subslice in self.slices[slice_ind].contours['endo_contours']])
			epi_contours.extend([epi_contours_subslice for epi_contours_subslice in self.slices[slice_ind].contours['epi_contours']])
			midpoint_epi_contour = np.array(self.slices[slice_ind].contours['epi_contours'][0])
			center_epi_point = np.mean(midpoint_epi_contour, axis=0)
			cur_midpoint_contour = np.subtract(np.array(self.slices[slice_ind].contours['endo_contours'][0]), center_epi_point)
			midpoint_endo_contour.append(cur_midpoint_contour)
			cur_midpoint_pinpoints = np.subtract(np.array(self.slices[slice_ind].contours['pinpoints']), center_epi_point)
			midpoint_pinpoints.append(cur_midpoint_pinpoints)
			pixel_mm_ratios.extend([self.slices[slice_ind].contours['image_resolution']]*self.slices[slice_ind].num_slices)
		# Shift slices to align the contours in X and Y (in-slice should be similar, across slices will vary).
		shifted_epi_contours = [[] for i in range(len(epi_contours))]
		shifted_endo_contours = [[] for i in range(len(endo_contours))]
		for contour_ind in range(len(epi_contours)):
			epi_contour_x = [epi_point[0] for epi_point in epi_contours[contour_ind]]
			epi_contour_y = [epi_point[1] for epi_point in epi_contours[contour_ind]]
			center_epi_point = [np.mean(epi_contour_x), np.mean(epi_contour_y)]
			shifted_epi_x = [epi_contour_x_val - center_epi_point[0] for epi_contour_x_val in epi_contour_x]
			shifted_epi_y = [epi_contour_y_val - center_epi_point[1] for epi_contour_y_val in epi_contour_y]
			shifted_epi_contours[contour_ind] = [(shifted_epi_x[i], shifted_epi_y[i]) for i in range(len(shifted_epi_x))]
			endo_contour_x = [endo_point[0] for endo_point in endo_contours[contour_ind]]
			endo_contour_y = [endo_point[1] for endo_point in endo_contours[contour_ind]]
			shifted_endo_x = [endo_contour_x_val - center_epi_point[0] for endo_contour_x_val in endo_contour_x]
			shifted_endo_y = [endo_contour_y_val - center_epi_point[1] for endo_contour_y_val in endo_contour_y]
			shifted_endo_contours[contour_ind] = [(shifted_endo_x[i], shifted_endo_y[i]) for i in range(len(shifted_endo_x))]
		# Calculate the mid-septal point from a selected slice.
		self.septal_points = []
		for slice_ind in range(len(self.slice_positions)):
			self.septal_points.extend([mathhelper.findConfMidPt(midpoint_pinpoints[slice_ind], midpoint_endo_contour[slice_ind])]*self.slices[slice_ind].num_slices)
		# Convert contours into real (mm) values and append slice height information.
		self.full_endo_contour = np.empty([0, 3])
		self.full_epi_contour = np.empty([0, 3])
		self.septal_pinpoints = []
		temp_disp_axes = False
		for subslice_ind in range(len(slice_heights)):
			endo_contour_arr = np.column_stack(([slice_heights[subslice_ind]]*len(endo_contours[subslice_ind]), np.array(shifted_endo_contours[subslice_ind])))
			endo_contour_arr[:, 1:] *= pixel_mm_ratios[subslice_ind]
			if endo_contour_arr.shape[1] == 3:
				self.full_endo_contour = np.vstack((self.full_endo_contour, endo_contour_arr))
			epi_contour_arr = np.column_stack(([slice_heights[subslice_ind]]*len(epi_contours[subslice_ind]), np.array(shifted_epi_contours[subslice_ind])))
			epi_contour_arr[:, 1:] *= pixel_mm_ratios[subslice_ind]
			if epi_contour_arr.shape[1] == 3:
				self.full_epi_contour = np.vstack((self.full_epi_contour, epi_contour_arr))
			pinpoint_arr = np.column_stack(([slice_heights[subslice_ind]]*len(self.septal_points[subslice_ind]), self.septal_points[subslice_ind]))
			pinpoint_arr[:, 1:] *= pixel_mm_ratios[subslice_ind]
			self.septal_pinpoints.append(pinpoint_arr)
			
		# Generate Apex-Base points for use in vertical alignment.
		self.basal_pt = np.array([np.min(slice_heights), 0, 0])
		self.apex_pt = np.array([np.max(slice_heights), 0, 0])
		prior_to_rot_points = [np.copy(pinpoint_val) for pinpoint_val in self.septal_pinpoints]
		for slice_ind in range(0, len(slice_heights)):
			self.full_epi_contour, self.full_endo_contour, self.septal_pinpoints[slice_ind] = confocalhelper.rotateContours(self.full_epi_contour, self.full_endo_contour, self.septal_pinpoints[slice_ind], self.septal_pinpoints[0], slice_heights[slice_ind])
		
		return(self.full_endo_contour, self.full_epi_contour, self.apex_pt, self.basal_pt, self.septal_pinpoints, prior_to_rot_points)
	
	def importSliceFluorescences(self, image_dir, channel_name):
		self.fluo_images[channel_name] = natsorted(glob.glob(os.path.join(image_dir, '*.tif')).copy(), alg=ns.IC)
		full_slice_arrs = [None for i in range(len(self.slices))]
		for slice_ind, conf_slice in enumerate(self.slices):
			full_slice_arrs[slice_ind] = conf_slice.importFluorescenceData(channel_name, self.fluo_images[channel_name][slice_ind])
		return(full_slice_arrs)
	
class ConfocalSlice():
	"""Class to hold information for a single biological slice of tissue.
	
	Biological slices are stored in individual directories.
	"""
	def __init__(self, stitching_dir=False, slice_width = 0.5):
		self.slice_width = slice_width
		self.slice_positions = None
		self.stitching_dir = stitching_dir
		self.stitching_images = None
		self.raw_images = None
		self.data_imported = False
		self.num_slices = 0
		self.contours = {}
		self.stitched_file = None
		self.slice_name = ''
		self.stitched_image = None
		self.channels = None
		self.compressed_images = None
		self.image_grid_file = None
		self.tif_files = None
		self.model_images = None
		self.ome_files = False
		if stitching_dir:
			self.stitching_images = self.setStitchingDirectory(stitching_dir)
	
	def setStitchingDirectory(self, stitching_dir):
		# Store the directory of images used for this model and parse out the folder name.
		self.stitching_dir = stitching_dir
		self.slice_name = os.path.split(stitching_dir)[1]
		
		# Record filenames for all tiff image files in the directory.
		self.tif_files = natsorted(glob.glob(os.path.join(stitching_dir, '*.tif')).copy(), alg=ns.IC)
		if len(self.tif_files) == 0:
			self.tif_files = natsorted(glob.glob(os.path.join(stitching_dir, '*.tiff')).copy(), alg=ns.IC)
			if len(self.tif_files) > 0:
				self.ome_files = True
			else:
				return(False)
		self.raw_images = [None]*len(self.tif_files)
		
		# Iterate through and create image objects for each tiff file
		for file_num, image_file in enumerate(self.tif_files):
			self.raw_images[file_num] = Image.open(image_file)
		
		# Get number of z-slices and channels based on either raw image information or OME XML Data
		if self.ome_files:
			self.channels, self.num_slices = confocalhelper.getOMEData(self.tif_files[0])
		else:
			self.num_slices = self.raw_images[0].n_frames
			self.channels = self.raw_images[0].getbands()
			
		# Define how many sub-slices (confocal slices) are contained within the overall slice
		self.slice_positions = [slice_num*(self.slice_width / self.num_slices) for slice_num in range(self.num_slices)]
		
		# Set up a list to hold compressed versions of the images, formatted as slices at top level
		#		This is due to compression only working on a single slice
		self.compressed_images = [None]*self.num_slices
		
		# Assign a file to contain positional information for the images for stitching purposes
		#		This file may or may not exist already in the directory, but is either referenced or created during stitching
		self.image_grid_file = self.stitching_dir + '/stitch_grid.txt'
		return(True)
	
	def setStitchedImage(self, stitched_image):
		self.stitched_file = stitched_image
		self.slice_name = os.path.split(stitched_image)[-1].split('.')[0]
		self.stitched_image = Image.open(self.stitched_file)
		self.num_slices = self.stitched_image.n_frames
		self.slice_positions = [slice_num*(self.slice_width / self.num_slices) for slice_num in range(self.num_slices)]
		self.__setDefaultKeyValues()
		return(True)
	
	def getStitchedSubslice(self, subslice_ind):
		try:
			image_frames = confocalhelper.splitImageFrames(self.stitched_image)
			selected_frame = image_frames[subslice_ind] if subslice_ind < self.num_slices else image_frames[0]
			return(selected_frame)
		except:
			return(False)
	
	def createStitchedImage(self, channel_num=0, overlap=0.1, compress_ratio=1, frame=0, stitched_file=False, force_file=False):
		"""Stitch images together in a grid.
		"""
		# Either read or create the image grid file
		if os.path.isfile(self.image_grid_file):
			self.im_grid, self.im_resolutions = confocalhelper.readImageGrid(self.image_grid_file)
		else:
			# If the image grid file does not exist, create it for future use.
			self.im_locs, im_locs_dict = confocalhelper.getImagePositions(self.tif_files)
			self.im_grid, self.im_resolutions = confocalhelper.getImageGrid(self.tif_files, self.im_locs, im_locs_dict)
			confocalhelper.writeImageGrid(self.im_grid, self.im_resolutions, self.image_grid_file)
		
		# Define a name for the file to use to store the stitched image
		if not stitched_file:
			stitched_file = self.confocal_dir + '/StitchedImages/' + self.slice_name + 'slice' + str(frame) + 'chan' + str(channel_num) + '.tif'
		if os.path.isfile(stitched_file) and not force_file:
			# If the file exists and overwriting isn't forced, don't bother stitching
			print('File exists! No need to overwrite.')
			return(True)
		
		# If there is nothing in the compressed images (i.e. it hasn't already been compressed), instantiate it
		if not self.compressed_images[frame]:
			self.compressed_images[frame] = [None]*len(self.raw_images)
		
		# Iterate through the images to generate compressed versions
		image_desc = False
		for image_num, raw_image in enumerate(self.raw_images):
			# Pull the desired frame
			if self.ome_files:
				# Adjust for the frame-channel shift with OME Tiff files
				image_frame_channel = confocalhelper.splitImageFrames(raw_image)[channel_num*self.num_slices+frame]
				if not image_desc:
					image_desc = confocalhelper.changeDescriptionScales(confocalhelper.getImageDescription(self.tif_files[image_num]), self.im_resolutions, compress_ratio, ome_file=True)
			else:
				image_frame = confocalhelper.splitImageFrames(raw_image)[frame] if frame < raw_image.n_frames else confocalhelper.splitImageFrames(raw_image)[0]
				# Split the frame into the desired channel
				image_frame_channel = image_frame.split()[channel_num]
				if not image_desc:
					image_desc = confocalhelper.changeDescriptionScales(confocalhelper.getImageDescription(self.tif_files[image_num]), self.im_resolutions, compress_ratio, ome_file=False)
			# If being told to compress the images, compress to desired ratio
			compressed_frame = confocalhelper.compressImages(image_frame_channel, image_scale=compress_ratio)
			self.compressed_images[frame][image_num] = compressed_frame

		# Pass the compressed images, image grid information, and stitched file to save to the image stitching function
		stitched_success = confocalhelper.stitchImages(self.compressed_images[frame], self.im_grid[:, 0], self.im_grid[:, 1], save_pos=stitched_file, description=image_desc)
		
	def createImageGridFile(self):
		im_locs, im_locs_dict = confocalhelper.getImagePositions(self.tif_files)
		im_grid = confocalhelper.getImageGrid(self.tif_files, im_locs, im_locs_dict)
		confocalhelper.writeImageGrid(im_grid, self.image_grid_file)
	
	def importStitchedImage(self, image_file):
		self.model_images = confocalhelper.openModelImage(image_file)
	
	def importModelData(self, data_file):
		model_dict = {}
		self.slice_name = os.path.splitext(os.path.basename(data_file))[0]
		with open(data_file, 'r') as import_file:
			for file_line in import_file.readlines():
				split_line = file_line.split(':')
				data_input = split_line[1].strip()
				try:
					model_dict[split_line[0]] = float(data_input)
				except:
					subslice_split = [subslice.strip('[] ') for subslice in data_input.split('],')]
					contour_points = [[] for i in range(len(subslice_split))]
					for subslice_ind, subslice_split_string in enumerate(subslice_split):
						tuple_split = [tuple([float(contour_pt_val.strip()) for contour_pt_val in tup_split.strip('() ').split(',')]) for tup_split in subslice_split_string.split('),') if len(tup_split) > 0]
						contour_points[subslice_ind] = tuple_split
					model_dict[split_line[0]] = contour_points
		self.contours = model_dict
		self.num_slices = len(self.contours['endo_contours'])
		self.slice_positions = [slice_num*(self.slice_width / self.num_slices) for slice_num in range(self.num_slices)]
		self.__setDefaultKeyValues()
		self.data_imported = True
		return(model_dict)
	
	def importFluorescenceData(self, channel_name, image_file):
		if not self.data_imported:
			return(False)
		fluorescence_image = Image.open(image_file)
		arr_list = [None for i in range(fluorescence_image.n_frames)]
		mask_locs_list = [None for i in range(fluorescence_image.n_frames)]
		for slice_num in range(fluorescence_image.n_frames):
			fluorescence_image.seek(slice_num)
			slice_endo_contour = self.contours['endo_contours'][slice_num]
			slice_epi_contour = self.contours['epi_contours'][slice_num]
			mask_image = Image.new(mode='L', size=fluorescence_image.size)
			mask_image_draw = ImageDraw.Draw(mask_image)
			if len(slice_epi_contour):
				mask_image_draw.polygon(slice_epi_contour, fill=255)
			if len(slice_endo_contour):
				mask_image_draw.polygon(slice_endo_contour, fill=0)
			mask_arr = np.array(mask_image)
			mask_locs_list[slice_num] = np.where(mask_arr)
			arr_list[slice_num] = np.array(fluorescence_image)[mask_locs_list[slice_num]]
		return(mask_locs_list, arr_list)
	
	def __setDefaultKeyValues(self):
		contour_keys = ['endo_contours', 'epi_contours', 'endo_resized_contours', 'epi_resized_contours']
		for contour_key in contour_keys:
			if not contour_key in self.contours:
				self.contours[contour_key] = [[] for i in range(self.num_slices)]
		other_keys = ['pinpoints', 'resized_pinpoints']
		for other_key in other_keys:
			if not other_key in self.contours:
				self.contours[other_key] = []
			else:
				if isinstance(self.contours[other_key][0], list):
					self.contours[other_key] = self.contours[other_key][0]
		return(True)
	
	def exportModelData(self, file_name):
		export_dict = {}
		for var_name, var_value in self.contours.items():
			var_value_edited = re.sub('  +', ' ', str(var_value).replace('\n','')).replace(' ]', ']').replace('] [', '][')
			export_dict[var_name] = var_value_edited
		with open(file_name, 'w+') as export_file:
			export_file.writelines([var_name + ':' + var_value + '\n' for var_name, var_value in export_dict.items()])